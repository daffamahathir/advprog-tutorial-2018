package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    public abstract void display();

    public void swim(){
    	System.out.println("Swims");
    }

    public void setFlyBehavior(FlyBehavior newFlyBehavior){
    	this.flyBehavior = newFlyBehavior;
    }

    public void setQuackBehavior(QuackBehavior newQuackBehavior){
    	this.quackBehavior = newQuackBehavior;
    }

    // TODO Complete me!
}
